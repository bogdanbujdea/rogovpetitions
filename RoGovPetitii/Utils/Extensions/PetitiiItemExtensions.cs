﻿using System;
using System.Collections.Generic;
using RoGovPetitii.GovOpenDataService;
using RoGovPetitii.Models;

namespace RoGovPetitii.Utils.Extensions
{
    public static class PetitiiItemExtensions
    {
        public static Petition ConvertToPetition(this PetitiiItem item)
        {
            var petition = new Petition
                {
                    Title = item.titlu,
                    Content = item.textpetitie,
                    Categories = new List<string> {item.categorie},
                    TargetSignatures = 2000,
                    TargetDate = DateTime.Now,
                    UserID = item.iduser,
                    ID = item.idpetitie,
                    SignaturesCount = item.nrvoturi
                };

            if (item.data != null)
            {
                petition.Date = (DateTime)item.data;
            }

            petition.RemainingVotes = petition.TargetSignatures - petition.SignaturesCount;

            return petition;
        }
    }
}
