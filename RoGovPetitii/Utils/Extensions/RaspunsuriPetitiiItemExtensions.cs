﻿namespace RoGovPetitii.Utils.Extensions
{
    using System;

    using Models;
    using GovOpenDataService;

    public static class RaspunsuriPetitiiItemExtensions
    {
        public static PetitionResponse ToPetitionResponse(this RaspunsuriPetitiiItem itemResponse)
        {
            var response = new PetitionResponse
                {
                    Title = itemResponse.titlu,
                    Response = itemResponse.raspuns,
                    EntityId = itemResponse.entityid,
                    Respondent = itemResponse.respondent,
                    PetitionID = itemResponse.idpetitie
                };

            if (itemResponse.data != null)
            {
                response.Date = (DateTime)itemResponse.data;
            }
            return response;
        }
    }
}
