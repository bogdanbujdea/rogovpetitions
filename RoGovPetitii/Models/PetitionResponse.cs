﻿namespace RoGovPetitii.Models
{
    using System;

    public class PetitionResponse
    {

        public DateTime Date { get; set; }

        public Guid EntityId { get; set; }

        public long? PetitionID { get; set; }

        public string Response { get; set; }

        public string Respondent { get; set; }

        public string Title { get; set; }
       
    }
}
