﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using RoGovPetitii.Annotations;
using RoGovPetitii.ViewModels;

namespace RoGovPetitii.Models
{
    
    public class Petition: INotifyPropertyChanged
    {
        private string _statusMessage;

        public Petition()
        {
            Image = new Uri("ms-appx:///Assets/AppImages/scroll.png").ToString();
            StatusMessage = "fără răspuns";
        }

        public string Title { get; set; }

        public List<string> Categories { get; set; }

        public string Content { get; set; }

        public int? SignaturesCount { get; set; }

        public DateTime Date { get; set; }

        public long? ID { get; set; }

        public int? TargetSignatures { get; set; }

        public DateTime? TargetDate { get; set; }

        public long? UserID { get; set; }

        public string Image { get; set; }

        public int? RemainingVotes { get; set; }

        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                if (value == _statusMessage) return;
                _statusMessage = value;
                OnPropertyChanged();
            }
        }

        public PetitionResponse Response { get; set; }

        public PetitionStatus Status { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
