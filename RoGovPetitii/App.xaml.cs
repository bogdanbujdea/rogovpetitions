﻿using RoGovPetitii.Repositories;
using RoGovPetitii.ViewModels;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;

namespace RoGovPetitii
{
    using System;
    using System.Collections.Generic;

    using Windows.UI.Xaml.Controls;
    using Windows.ApplicationModel.Activation;
    
    using Caliburn.Micro;

    using Views;
    
    public sealed partial class App
    {
        
        private WinRTContainer _container;

        public App()
        {
            InitializeComponent();
        }

        protected override void Configure()
        {
            _container = new WinRTContainer();
            _container.RegisterWinRTServices();

            _container.RegisterPerRequest(typeof(IPetitionRepository), "IPetitionRepository", typeof(PetitionRepository));
            _container.RegisterPerRequest(typeof(IPetitionResponseRepository), "IPetitionResponseRepository", typeof(PetitionResponseRepository));
            _container.PerRequest<PetitionsViewModel>();
            _container.PerRequest<PetitionDetailViewModel>();
            
            _container.RegisterSingleton(typeof(IDataService), "IDataService", typeof(DataService));

        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;
            throw new Exception("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            _container.RegisterNavigationService(rootFrame);
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            SettingsPane.GetForCurrentView().CommandsRequested += SettingsCommandsRequested;
            DisplayRootView<PetitionsView>();
        }

        private void SettingsCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            args.Request.ApplicationCommands.Add(new SettingsCommand("privacy", "Privacy", async command =>
                {
                    await
                        new MessageDialog(
                            "Deoarece folosim conexiunea dvs. la internet, suntem obligați să vă afișăm o politică de confidențialitate. Aplicația nu va colecta niciun fel de date despre dvs. Singurul motiv pentru care folosim conexiunea la internet este pentru a descărca petițiile și pentru a vă permite să le distribuiți prin contractul de share")
                            .ShowAsync();
                }));
        }
    }
}
