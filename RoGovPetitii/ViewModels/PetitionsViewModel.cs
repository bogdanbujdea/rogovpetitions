﻿using System;
using System.Threading.Tasks;
using Windows.UI.Popups;
using System.Linq;

namespace RoGovPetitii.ViewModels
{
    using System.Collections.ObjectModel;
    
    using Caliburn.Micro;

    using Models;
    using Repositories;

    public enum PetitionStatus
    {
        Answered,
        Unanswered,
        Opened,
        Closed
    }

    public class PetitionsViewModel: ViewModelBase, IPetitionsViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IPetitionRepository _petitionRepository;
        private readonly IPetitionResponseRepository _petitionResponseRepository;
        private readonly IDataService _dataService;
        private ObservableCollection<Petition> _petitions;
        private ObservableCollection<Petition> _petitionsCopy;        

        public PetitionsViewModel(INavigationService navigationService, IPetitionRepository petitionRepository, IPetitionResponseRepository petitionResponseRepository, IDataService dataService) : base(navigationService)
        {
            _navigationService = navigationService;
            _petitionRepository = petitionRepository;
            _petitionResponseRepository = petitionResponseRepository;
            _dataService = dataService;
            Petitions = new ObservableCollection<Petition>();
        }

        protected override async void OnInitialize()
        {
            base.OnInitialize();
            await LoadPetitions();
        }

        public async void RefreshPetitions()
        {
            await LoadPetitions();
        }

        private async Task LoadPetitions()
        {
            var errorMessage = "";
            try
            {
                Petitions = new ObservableCollection<Petition>(await _petitionRepository.RetrieveAllPetitions());
                var responses = await _petitionResponseRepository.RetrieveAllPetitionResponses();
                foreach (var petition in Petitions)
                {
                    var response = responses.FirstOrDefault(r => r.PetitionID == petition.ID);
                    if (response != null)
                    {
                        petition.StatusMessage = "are răspuns";
                        petition.Status = PetitionStatus.Answered;
                    }
                    else
                    {
                        petition.StatusMessage = "fără răspuns";
                        petition.Status = PetitionStatus.Unanswered;
                    }
                }
                _petitionsCopy = Petitions;
            }
            catch (Exception)
            {
                errorMessage = "Nu putem descărca petițiile. Vă rugăm încercați din nou.";
            }
            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                await new MessageDialog(errorMessage).ShowAsync();
            }
        }

        public ObservableCollection<Petition> Petitions
        {
            get { return _petitions; }
            set
            {
                if (Equals(value, _petitions)) return;
                _petitions = value;
                NotifyOfPropertyChange();
            }
        }

        public bool DateSort { get; set; }

        public bool VoteSort { get; set; }

        #region Events

        public void ItemSelectionChanged(object item)
        {
            var petition = item as Petition;
            if (petition != null)
            {
                _dataService.Data = petition;
                _navigationService.NavigateToViewModel<PetitionDetailViewModel>();
            }
        }


        public void ListItemSelectionChanged(object item)
        {
            var petition = item as Petition;
            if (petition != null)
            {
                _dataService.Data = petition;
                _navigationService.NavigateToViewModel<PetitionDetailViewModel>();
            }
        }
        #endregion

        public void FilterByAnsweredPetitions()
        {
            Petitions = new ObservableCollection<Petition>(Petitions.Where(p => p.Status == PetitionStatus.Answered).ToList());
        }

        public void FilterByAllPetitions()
        {
            Petitions = _petitionsCopy;
        }

        public void SortByDate()
        {
            if (!DateSort)
            {
                Petitions = new ObservableCollection<Petition>(Petitions.OrderBy(p => p.Date).ToList());
            }
            else
            {
                Petitions = new ObservableCollection<Petition>(Petitions.OrderByDescending(p => p.Date).ToList());
            }
            DateSort = !DateSort;
        }

        public void SoryByVotes()
        {
            if (!VoteSort)
            {
                Petitions = new ObservableCollection<Petition>(Petitions.OrderBy(p => p.SignaturesCount).ToList());
            }
            else
            {
                Petitions = new ObservableCollection<Petition>(Petitions.OrderByDescending(p => p.SignaturesCount).ToList());
            }
            VoteSort = !VoteSort;
        }
    }
}
