﻿using Windows.ApplicationModel.DataTransfer;

namespace RoGovPetitii.ViewModels
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Windows.System;

    using Caliburn.Micro;

    using Models;
    using Repositories;

    public class PetitionDetailViewModel : ViewModelBase, IPetitionDetailViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IDataService _dataService;
        private readonly IPetitionResponseRepository _petitionResponseRepository;
        private Petition _petition;
        private string _formattedDate;
        private PetitionResponse _petitionResponse;
        private bool _hasResponse;
        private string _title;
        private string _content;
        private string _date;
        private bool petitionLoaded;
        private string _category;

        public PetitionDetailViewModel(INavigationService navigationService, IDataService dataService, IPetitionResponseRepository petitionResponseRepository)
            : base(navigationService)
        {
            _navigationService = navigationService;
            _dataService = dataService;
            _petitionResponseRepository = petitionResponseRepository;
            if (Execute.InDesignMode)
            {
                LoadFakePetition();
            }
            AppBarIsOpen = true;
            petitionLoaded = true;
        }


        #region Model

        public string Title
        {
            get { return _title; }
            set
            {
                if (value == _title) return;
                _title = value;
                NotifyOfPropertyChange();
            }
        }

        public string Content
        {
            get { return _content; }
            set
            {
                if (value == _content) return;
                _content = value;
                NotifyOfPropertyChange();
            }
        }

        public string Date
        {
            get { return _date; }
            set
            {
                if (value == _date) return;
                _date = value;
                NotifyOfPropertyChange();
            }
        }

        public string Category
        {
            get { return _category; }
            set
            {
                if (value == _category) return;
                _category = value;
                NotifyOfPropertyChange();
            }
        }

        #endregion

        public PetitionResponse PetitionResponse
        {
            get { return _petitionResponse; }
            set
            {
                if (Equals(value, _petitionResponse)) return;
                _petitionResponse = value;
                NotifyOfPropertyChange();
            }
        }

        private void LoadFakePetition()
        {
            Petition = new Petition();
            Petition.Title = "Titlu petitie ";
            Petition.SignaturesCount = 99;
            Petition.Content = "lorem ipsum";
            Petition.Date = DateTime.Now.Add(TimeSpan.FromDays(4));
            var categories = new List<string> { "AGRICULTURĂ", "ECONOMIE", "SĂNĂTATE", "COMERȚ", "APĂRARE", "ENERGIE", "DREPTURILE OMULUI" };
            Petition.Categories = categories.TakeWhile(c => c.Length % 2 != 0).Take(3).ToList();
            Petition = Petition;
        }

        public bool HasResponse
        {
            get { return _hasResponse; }
            set
            {
                if (value.Equals(_hasResponse)) return;
                _hasResponse = value;
                NotifyOfPropertyChange();
            }
        }

        protected override async void OnInitialize()
        {
            base.OnInitialize();
            var petition = _dataService.Data as Petition;
            Petition = petition;
            
            if (Petition != null)
            {
                LoadData(Petition.Title, Petition.Date.ToString("d.MM.yyyy, HH:mm"), Petition.Categories.FirstOrDefault(), Petition.Content);
            }

            var responses = await _petitionResponseRepository.RetrieveAllPetitionResponses();
            PetitionResponse = responses.FirstOrDefault(r => r.PetitionID == Petition.ID);
            HasResponse = PetitionResponse != null;
        }

        public void LoadData(string title, string date, string category, string content)
        {
            Title = title;
            Date = date;
            Category = category;
            Content = content;
        }
        
        public void ChangePetitionView()
        {
            if (petitionLoaded)
            {
                LoadData(PetitionResponse.Title,
                    PetitionResponse.Date.ToString("d.MM.yyyy, HH:mm"),
                    Petition.Categories.FirstOrDefault(),
                    PetitionResponse.Response);
            }
            else
            {
                LoadData(Petition.Title,
                    Petition.Date.ToString("d.MM.yyyy, HH:mm"),
                    Petition.Categories.FirstOrDefault(),
                    Petition.Content);
            }
            petitionLoaded = !petitionLoaded;
        }

        public Petition Petition
        {
            get { return _petition; }
            set
            {
                if (Equals(value, _petition)) return;
                _petition = value;
                NotifyOfPropertyChange();
            }
        }

        public bool AppBarIsOpen { get; set; }

        public async void ViewOnline()
        {
            var uri = new Uri("http://hackrogov.cloudapp.net/petitii/search/view_petitie/" + Petition.ID);
            var success = await Launcher.LaunchUriAsync(uri);
            if (success)
            {
                AppBarIsOpen = false;
            }
            else
            {
                AppBarIsOpen = true;
            }
        }

        public void Share()
        {
            DataTransferManager.ShowShareUI();
        }
    }
}
