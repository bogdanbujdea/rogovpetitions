﻿namespace RoGovPetitii.ViewModels
{
    public interface IPetitionsViewModel
    {
        void FilterByAnsweredPetitions();
        void FilterByAllPetitions();
        void SortByDate();
        void SoryByVotes();
    }
}
