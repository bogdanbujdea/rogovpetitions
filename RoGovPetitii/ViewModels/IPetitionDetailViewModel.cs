﻿namespace RoGovPetitii.ViewModels
{
    
    using Models;

    public interface IPetitionDetailViewModel
    {
        PetitionResponse PetitionResponse { get; set; }
        Petition Petition { get; set; }

        void LoadData(string title, string date, string category, string content);
    }
}
