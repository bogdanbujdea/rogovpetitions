﻿using RoGovPetitii.ViewModels;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using System.Linq;
using Windows.UI.Xaml.Automation;

namespace RoGovPetitii.Views
{
    using System;
    using System.Collections.Generic;

    public sealed partial class PetitionDetailView
    {
        private IPetitionDetailViewModel _viewModel;

        public PetitionDetailView()
        {
            InitializeComponent();
        }

        
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // Allow saved page state to override the initial item to display
            if (pageState != null && pageState.ContainsKey("SelectedItem"))
            {
                navigationParameter = pageState["SelectedItem"];
            }
            _viewModel = DataContext as IPetitionDetailViewModel;
            // TODO: Assign a bindable group to this.DefaultViewModel["Group"]
            // TODO: Assign a collection of bindable items to this.DefaultViewModel["Items"]
            // TODO: Assign the selected item to this.flipView.SelectedItem
        }


        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            // TODO: Derive a serializable navigation parameter and assign it to pageState["SelectedItem"]
        }

        #region ShareContract

        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            UnregisterForShare();
        }

        protected override void OnNavigatedTo(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            RegisterForShare();
        }

        private void RegisterForShare()
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();

            dataTransferManager.DataRequested += ShareStorageItemsHandler;
        }

        private void UnregisterForShare()
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested -= ShareStorageItemsHandler;
        }

        private async void ShareStorageItemsHandler(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;
            request.Data.Properties.Title = "Distribuie această petiție";
            request.Data.Properties.Description = "Alege una din aplicațiile de mai jos";

            DataRequestDeferral deferral = request.GetDeferral();

            try
            {
                var uri = new Uri("http://hackrogov.cloudapp.net/petitii/search/view_petitie/" + _viewModel.Petition.ID);
                request.Data.SetUri(uri);
            }
            catch (Exception)
            {
                e.Request.FailWithDisplayText("Nu putem distribui petiția în acest moment. Vă rugăm încercați mai târziu!");
            }
            finally
            {
                deferral.Complete();
            }
        }

        #endregion

        private bool animation = true;
        private void OnViewResponse(object sender, RoutedEventArgs e)
        {
            if (animation)
            {
                CardRotation.Begin();
                ChangePetitionView.SetValue(AutomationProperties.NameProperty, "Vizionează petiția");
            }
            else
            {
                CardRotationBack.Begin();
                ChangePetitionView.SetValue(AutomationProperties.NameProperty, "Vizionează răspunsul");                
            }
            animation = !animation;
        }
    }
}