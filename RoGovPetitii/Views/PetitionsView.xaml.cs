﻿using RoGovPetitii.ViewModels;
using Windows.Foundation;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace RoGovPetitii.Views
{
    using System;
    using System.Collections.Generic;

    public sealed partial class PetitionsView
    {
        private IPetitionsViewModel _viewModel;

        public PetitionsView()
        {
            InitializeComponent();
        }

       
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            _viewModel = DataContext as IPetitionsViewModel;
            // TODO: Assign a collection of bindable groups to this.DefaultViewModel["Groups"]
        }

        private async void OnFilterItems(object sender, RoutedEventArgs e)
        {
            var p = new PopupMenu();
            p.Commands.Add(new UICommand("Petiții cu răspuns", (command) => ShowAnsweredPetitions()));
            p.Commands.Add(new UICommand("Toate petițiile", (command) => ShowAllPetitions()));
            await p.ShowForSelectionAsync(GetRect(sender), Placement.Above);
        }

        private void ShowAllPetitions()
        {
            _viewModel.FilterByAllPetitions();
        }

        private void ShowAnsweredPetitions()
        {
            _viewModel.FilterByAnsweredPetitions();
        }

        private Rect GetRect(object sender)
        {
            var element = sender as FrameworkElement;
            if (element != null)
            {
                GeneralTransform elementTransform = element.TransformToVisual(null);
                Point point = elementTransform.TransformPoint(new Point());
                return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
            }
            return new Rect();
        }

        private async void OnSortItems(object sender, RoutedEventArgs e)
        {
            var p = new PopupMenu();
            p.Commands.Add(new UICommand("După data creării", (command) => SortByDate()));
            p.Commands.Add(new UICommand("După numărul de voturi", (command) => SoryByVotes()));
            await p.ShowForSelectionAsync(GetRect(sender), Placement.Above);
        }

        private void SoryByVotes()
        {
            _viewModel.SoryByVotes();
        }

        private void SortByDate()
        {
            _viewModel.SortByDate();
        }
    }
}
