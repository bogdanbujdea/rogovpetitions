﻿using System;
using Windows.ApplicationModel.Resources;

namespace RoGovPetitii.Resources
{
    public static class AppConfiguration
    {
        private static readonly ResourceLoader ResourceLoader = new ResourceLoader("AppResources");

        public static class ServiceEndpoint
        {
            public static Uri BaseUri
            {
                get
                {
                    return new Uri(ResourceLoader.GetString("ServiceEndpoint"));
                }
            }
        }
    }
}
