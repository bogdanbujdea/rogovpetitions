﻿namespace RoGovPetitii.Repositories
{
    public interface IDataService
    {
        object Data { get; set; }
    }
}
