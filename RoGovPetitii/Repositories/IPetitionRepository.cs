﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RoGovPetitii.GovOpenDataService;
using RoGovPetitii.Models;

namespace RoGovPetitii.Repositories
{
    public interface IPetitionRepository
    {
        Task<IEnumerable<Petition>> RetrieveAllPetitions();
        
    }
}
