﻿namespace RoGovPetitii.Repositories
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Data.Services.Client;

    using Models;
    using Resources;
    using Utils.Extensions;
    using GovOpenDataService;

    public class PetitionRepository: IPetitionRepository
    {
        private RoGovOpenDataDataService _service;

        public async Task<IEnumerable<Petition>> RetrieveAllPetitions()
        {
            _service = new RoGovOpenDataDataService(AppConfiguration.ServiceEndpoint.BaseUri);
            
            var query = (from petitiiItem in _service.Petitii
                        select petitiiItem) as DataServiceQuery<PetitiiItem>;
            var allPetitions = await RetrievePetitions(query);
            return allPetitions;
        }

        private async Task<List<Petition>> RetrievePetitions(DataServiceQuery<PetitiiItem> query)
        {
            IEnumerable<PetitiiItem> petitions = await query.ExecuteAsync();
            
            var p = ConvertToPetitions(petitions.ToList());
            return p;
            return GetFakePetitions();
        }

        private List<Petition> GetFakePetitions()
        {
            var petitions = new List<Petition>();
            for (int i = 1; i <= 15; i++)
            {
                var categories = new List<string> { "AGRICULTURĂ", "ECONOMIE", "SĂNĂTATE", "COMERȚ", "APĂRARE", "ENERGIE", "DREPTURILE OMULUI" };
                var date = DateTime.Now.Add(TimeSpan.FromDays(i));
                var petition = new Petition();
                petition.Title = "Titlu petitie " + i;
                petition.SignaturesCount = i * 10;
                petition.Content = "Gama interventiilor la care este solicitat SMURD include toate urgentele ce pun viata unei persoane sau mai multora in pericol imediat. Acestea includ accidentele rutiere, exploziile, accidentele de munca sau casnice cum ar fi caderea de la inaltime sau electrocutarea, starile de inconstienta ce includ si stopurile cardiorespiratorii, suspiciunile de infarct, insuficientele respiratorii acute si nu in ultimul rind accidentele cu multiple victime. Echipajul se deplaseaza indiferent de virsta victimei, aproximativ 7% din cazurile noastre fiind copii sub varsta de 16 ani" + i;
                petition.Date = date;
                petition.ID = i * new Random().Next(0, i);
                petition.TargetDate = DateTime.Now.AddDays(i);
                petition.TargetSignatures = (i * i) + 250;
                petition.RemainingVotes = petition.TargetSignatures - petition.SignaturesCount;
                petition.UserID = i * 23;
                categories.Shuffle();
                petition.Categories = categories.Take(3).ToList();
                petitions.Add(petition);
            }
            petitions[0].SignaturesCount = petitions[0].TargetSignatures;
            petitions[0].Title =
                "Transparența și deschiderea spre partenerii sociali şi consultarea societăţii civile - See more at...";
            petitions[0].ID = 0;
            petitions[0].UserID = 100;
            return petitions;
        } 
        
        private List<Petition> ConvertToPetitions(List<PetitiiItem> petitii)
        {
            var petitions = new List<Petition>();
            foreach (var item in petitii)
            {
                var petition = new Petition();
                petition.Title = item.titlu;
                petition.Content = item.textpetitie;
                if (item.nrvoturi != null)
                {
                    petition.SignaturesCount = item.nrvoturi;
                }

                petition.ID = item.idpetitie;
                petition.Categories = new List<string>{item.categorie};
                petition.TargetSignatures = 2000;
                petition.TargetDate = DateTime.Now;
                petition.RemainingVotes = petition.TargetSignatures - petition.SignaturesCount;
                petition.UserID = item.iduser;

                if (item.data != null)
                {
                    petition.Date = (DateTime) item.data;
                }
                petitions.Add(petition);
            }
            return petitions;
        }
    }
}
