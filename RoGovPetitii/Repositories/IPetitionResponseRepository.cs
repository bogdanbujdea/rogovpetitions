﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RoGovPetitii.Models;

namespace RoGovPetitii.Repositories
{
    public interface IPetitionResponseRepository
    {
        Task<List<PetitionResponse>> RetrieveAllPetitionResponses();
        Task<PetitionResponse> RetrieveResponseForPetition(long? petitionID);
    }
}
