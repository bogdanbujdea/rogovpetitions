﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Threading.Tasks;
using RoGovPetitii.GovOpenDataService;
using RoGovPetitii.Models;
using RoGovPetitii.Resources;
using RoGovPetitii.Utils.Extensions;

namespace RoGovPetitii.Repositories
{
    public class PetitionResponseRepository: IPetitionResponseRepository
    {
        public async Task<List<PetitionResponse>> RetrieveAllPetitionResponses()
        {
            var service = new RoGovOpenDataDataService(AppConfiguration.ServiceEndpoint.BaseUri);

            var query = (from petitiiItem in service.RaspunsuriPetitii
                         select petitiiItem) as DataServiceQuery<RaspunsuriPetitiiItem>;

            var responses = await RetrievePetitionResponses(query);
            
            return responses;
        }

        public List<PetitionResponse> GetFakeResponses()
        {
            var responses = new List<PetitionResponse>();
            for (int i = 1; i <= 3; i++)
            {
                var date = DateTime.Now.Add(TimeSpan.FromDays(i));
                var response = new PetitionResponse();
                response.Title = "Titlu raspuns petitie " + i;
                response.Response = "Raspunsul este " + i;
                response.Date = date;
                response.PetitionID = i * i;
                response.Respondent = (i * 23).ToString();
                responses.Add(response);
            }
            responses[0].Title =
                "Raspuns foarte lung la prima petitie care de asemenea are un titlu foarte foarte lung";
            responses[0].PetitionID = 0;
            return responses;
        }

        private async Task<List<PetitionResponse>> RetrievePetitionResponses(DataServiceQuery<RaspunsuriPetitiiItem> query)
        {
            IEnumerable<RaspunsuriPetitiiItem> petitions = await query.ExecuteAsync();

            var p = ConvertToPetitionResponses(petitions.ToList());
            return p;
            return GetFakeResponses();
        }

        
        public async Task<PetitionResponse> RetrieveResponseForPetition(long? petitionID)
        {
            var service = new RoGovOpenDataDataService(AppConfiguration.ServiceEndpoint.BaseUri);

            var query = (from item in service.RaspunsuriPetitii
                         where item.idpetitie == petitionID
                         select item) as DataServiceQuery<RaspunsuriPetitiiItem>;

            var result = await query.ExecuteAsync();
            var items = result as IList<RaspunsuriPetitiiItem> ?? result.ToList();
            if (items.Any())
            {
                var response = items.FirstOrDefault();
                if (response != null)
                {
                    return response.ToPetitionResponse();
                }    
            }
            return null;

        }

        private List<PetitionResponse> ConvertToPetitionResponses(IEnumerable<RaspunsuriPetitiiItem> serviceResponses)
        {
            return (from item in serviceResponses where item != null select item.ToPetitionResponse()).ToList();

        }
    }
}
